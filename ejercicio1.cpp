#include <iostream>
#include <iomanip>
#include "Graph.h"

//int main() {
//    for (int i = 0; i < 6; ++i) {
//        string file;
////        file = "../instancias/instancia" + to_string(i) + ".txt";
//        file = "../geodesicas/instancia_" + to_string(i) + ".txt";
//        Graph G(file);
//        cout << file << endl;
//        bool res = G.esGeodesico();
////        cout << res << endl;
//    }
////    string file;
//////    file = "../instancias/instancia_" + to_string(0) + ".txt";
////    file = "/Users/noehsueh/Desktop/geodesicas/instancia_6.txt";
////    Graph G(file);
////    cout << file << endl;
////    G.esGeodesico();
//    return 0;
//}


int main(int argc, char** argv) {
    for (int i = 1; i < argc; ++i) {
        string file;
        file = argv[i];
        Graph G(file);
        cout << file << endl;
        G.esGeodesico();
    }
    return 0;
}